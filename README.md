# flutter_provider_starter_kit

A starter kit for an flutter app with provider as a sate managment tool.

## Getting Started

### Folders

#### 1. models

Use this folder for storing all of your model class which will used to parse the JSON and to store it.

#### 2. providers

This is the place where all of your providers take palce.
you can use feature wise approch or teh screen wise approch.

#### 3. services

- API services
